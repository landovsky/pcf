---
layout: event
class: post-template
navigation: True
title: Friday night outreach
event_date: 2017-12-31
location: Prague
start_time: 8:45pm
cover: assets/images/sky.jpg
tags: Street
---
Every Friday night, YWAM does ministry on the streets in the Red Light District in Prague. We welcome you to join us.

We usually meet around 8:45pm and minister until 10:00 or 10:30pm. If you would like to join us for this time of ministry, please write to Robin and Roger Harsh. rogerarobin@gmail.com