---
layout: event
class: post-template
subclass: 'post tag-fiction'
navigation: True
title: Homeless Ministry
event_date: 2018-12-22
location: Prague
start_time: "13.00"
end_time: 6pm
cover: assets/images/abraham.jpg
tags: Street
---
Every Thursday you can join with YWAM to reach out to the homeless at the main train station, Hlavni nadrazi.

If you would be interested in participating, please speak to Miranda for details (randilberg@gmail.com) and to let them know you are coming. If you have "stravenky" (food coupons) to donate, please put them in the offering basket. Thank you.