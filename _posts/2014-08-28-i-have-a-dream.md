---
layout: post
current: post
cover: assets/images/sky.jpg
navigation: True
title: Putting God First This Year
date: 2014-08-28 10:18:00
tags: Sermons
class: post-template
subclass: 'post tag-speeches'
author: ghost
---

We have started off to a great New Year. God is good and he is faithful. But, part of being good and faithful involves helping his kids grow up. Maybe the greatest lesson is to love God first – to make God the first priority in our lives. We can put that into tangible practice in our daily choices. It involves big and little decisions. A little decision might be do we watch that horror film. A big decision might be do we decide to establish Jesus as Lord of our finances. Both are important. However, the Bible says that the love of money is the root of all evil. Finances are a big door that unlock either a life of blessing or a life of toil. As such, we would be wise to pay attention to scripture and apply it to our lives.

> “On the first day of each week, you should each put aside a portion of the money you have earned. Don't wait until you > get there and then try to collect it all at once.”
*1 Corinthians 16:2 (NLT)*

- Giving should be periodic: “On the first day of the week...”
- Giving should be personal: “...you should each...”
- Giving should be out of a private savings: “...put aside a portion...”

> “Honor the Lord with your wealth and with the best part of everything you produce. Then he will fill your barns with grain, and your vats will overflow with good wine.”
*Proverbs 3:9-10 (NLT)*

- Giving should be unto the Lord: “Honor the Lord from your wealth…”
- Giving should be a priority: “from the first of all your produce”
- Giving will be reciprocal: “he will fill… your vats will overflow…”

>“You must each decide in your heart how much to give. And don’t give reluctantly or in response to pressure. ‘For God loves a person who gives cheerfully.’ And God will generously provide all you need. Then you will always have everything you need and plenty left over to share with others.”
*2 Corinthians 9:7-8 (NLT)*

- Giving should be premeditated: “You must decide in your heart how much to give.”
- Giving should be desirous: “don’t give reluctantly or in response to pressure.”
- Giving should be cheerful: “God loves a person who gives cheerfully.”
- Giving should be in faith: “God will generously provide… you will always have…”
- Giving should produce generosity: “plenty left over to share with others.”

> “When you give to someone in need, don’t do as the hypocrites do—blowing trumpets in the synagogues and streets to call attention to their acts of charity! I tell you the truth, they have received all the reward they will ever get. But when you give to someone in need, don’t let your left hand know what your right hand is doing. Give your gifts in private, and your Father, who sees everything, will reward you.”
*Matthew 6:2-4 (NLT)*

- Giving should be without pride: “to call attention to their acts of charity.”
- Giving should be private: “don’t let your left hand know… Give your gifts in private…”
- Giving will be rewarded: “your Father… will reward you.”

You will find the Bible has a lot more to say about money. Google says, “The Bible offers 500 verses on prayer, fewer than 500 verses on faith, and more than 2,000 verses on money. In fact, 15 percent of everything Jesus ever taught was on the topic of money and possessions - more than His teachings on heaven and hell combined.”

Yet, despite the overwhelming scriptural attention to the subject, a majority of the Body of Christ in our day do not tithe. It seems like if we are looking to mature in 2018, this might be an area worthy of our attention. Take the challenge – see if you can out give God this year.
Blessings,

*Pastor John*