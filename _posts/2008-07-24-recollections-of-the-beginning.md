---
layout: post
current: post
cover: 'assets/images/waves.jpg'
navigation: True
title: Recollections of the beginning
date: 2008-07-24 10:18:00
tags: Sermons
class: post-template
subclass: 'post tag-fiction'
author: drapal
---

Before the Velvet Revolution Larry Winnes used to come to Prague on behalf of John McFarlane, who was – and still is regarded to have been “an apostle” for Czechoslovakia. John used to live in Prague in the late sixties and in Slovakia in the early seventies prior to being expelled. Then he moved his headquarters to Nuremberg, Germany. He could not travel to the East himself (he would not be granted a visa) but he sent different people here. I do not remember when I met Larry first, but I am sure it was still in the Communist years (i.e. before November 1989). Before coming to Europe, Larry worked in Central America. 

After the Velvet Revolution Prague was flooded by Americans. Only a fraction of them were Christians. Many were drawn here by the atmosphere of the city which resembled western cities much less than it does now (the first MacDonald’s was opened only in January 1992). Food was unbelievably cheap for the foreigners from the West and so were the rents. Hundreds and possibly thousands of young Americans flocked to Prague to have a good time. Other people came, too (most prominently - businessmen). 

Křesťanské společenství Praha- "KSP" (Christian Fellowship Prague) was officially started in May 1990. Before that, we were part of the “Evangelical Church of Czech Brethren.” You have to keep in mind that “Evangelical” here has a different meaning than in United States. It means simply “Protestant” (like the German evangelisch). Our former denomination was not evangelical at all – it was mainstream Protestant church like many Presbyterian churches in the U.S. 

For several and different reasons we used to meet only once a month as a “whole congregation” (all of us in Prague) – on the first Sunday of each month. On other Sunday different parts of the church (we tried to call them “Clans” at first, but soon the word “Regions” prevailed) met in different “regions” of the city. On our “Big Sundays” (the first Sundays of the month) translation was provided for the English- speaking people. All the Regions were divided into home groups of a relatively small size (5-10). 

Starting an English-speaking fellowship wasn’t my idea. I must confess that I never was a visionary. The author of this idea was Larry Winnes, the first pastor of what then was the “English Region” (the name PCF was not used until John & Kelsie arrived to pastor the church). The English Region was structured differently from the beginning. It had only one home group which tended to be rather big. If I understood it well, the big home group provided a sort of substitute for a big family. The Americans and other foreigners felt the cultural difference very much. I suppose they felt it much more than today since the difference between Czech Republic and Western countries has diminished considerably. The group used to meet in a house the Winnes’ rented in Prague 6 (Petřiny). Good food was always provided.

The leaders of KSP also used to meet at the Winnes’ home. Each January, the KSP elders and some other leaders used to spend a week in the mountains fasting and talking and praying. Larry used to come, too, in spite of the language barrier. Larry loved the Czech church and deliberately and very decidedly wanted to be a part of the Czech church.

Larry was an asset to me for many reasons. One of them is probably not easy to understand now. In the first three years after the Velvet Revolution Eastern Europe was flooded by missionaries (in many cases the word “missionaries” would deserve quotation marks). Ninety percent of them had very little knowledge about Czechoslovakia and had practically no idea what would they like to do here. They all wanted to “help,” but meeting several of them each week was a nuisance - if I can be frank. Larry was a great help because I could send many of these people to meet with him.

Larry had a very soft heart. He was not a very good organizer, but he loved people very much. I remember he spent a whole day with a Russian illegal immigrant, helping him to get his teeth mended (and actually spending an awful lot of money on him). He was very empathic and tried to help people wherever he could. 

Larry & Jan had three children either in college or private school. That meant they needed a lot of money to pay for their schools. They also needed a car. Larry had an older Volkswagen Passat. One day the car was stolen. Larry prayed very much for the car to be found and we supported him in prayer. I must confess I did not have too much faith. Usually the stolen cars were driven over the border within three or four hours and very few of them were ever found. The police did not give Larry any hope. Nevertheless, they phoned him six days later that the car was found in another part of Prague. Only the handle was broken. Apart from God’s intervention, it might have been the non-functioning fuel indicator which saved Larry. Thieves did not like to stop at gas stations and if they saw the tank was empty, they left the car and stole another one (car theft was a big problem in those days). 

I will add one very personal story. Larry’s wife, Janice, celebrates her birthday at the end of August. One year we decided to give her a traditional Czech birthday cake. It was quite expensive but we had it made by a confectioner in the village where my wife lived in her childhood for half of the price it would cost in Prague. It was a huge three-layer cake with a lot of cream, nuts, chocolate, etc. We had to transport it in a car and air conditioning was unheard of back then. It was very hot indeed that day and the cake suffered a lot (we had to drive about 70 kilometers). But in spite of the cake losing some of its beauty (as you can imagine), Janice was moved to tears. 

There are many more stories… and most involve people who sacrificed and a faithful, loving God.

Yours for Calvary,

*Dan*